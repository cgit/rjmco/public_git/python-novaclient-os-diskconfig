%global srcname novaclient-os-diskconfig
%global upstreamname os_diskconfig_python_novaclient_ext

%if 0%{?fedora} >= 25
%global with_python3 1
%endif

Name:		python-%{srcname}
Version:	0.1.3
Release:	1%{?dist}
Summary:	Disk Config extension for python-novaclient

Group:		Development/Libraries
License:	ASL 2.0
URL:		https://pypi.python.org/pypi/%{upstreamname}
Source0:	https://files.pythonhosted.org/packages/source/o/%{upstreamname}/%{upstreamname}-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	python2-devel
%if 0%{?with_python3}
BuildRequires:	python3-devel
%endif

%description
%{summary}

%package -n python2-%{srcname}
Summary:	%{summary}
BuildRequires:	python-novaclient
Requires:	python-novaclient
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
%{summary}

%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:	%{summary}
BuildRequires:	python3-novaclient
Requires:	python3-novaclient
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
%{summary}
%endif

%prep
%autosetup -n %{upstreamname}-%{version}

%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif

%install
%py2_install
%if 0%{?with_python3}
%py3_install
%endif

%check
%{__python2} setup.py test
%if 0%{?with_python3}
%{__python3} setup.py test
%endif

%files -n python2-%{srcname}
%doc README.rst
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.rst
%{python3_sitelib}/*
%endif

%changelog
* Sun Jan 01 2017 Ricardo Cordeiro <gryfrev8-redhat.com-rjmco@tux.com.pt> 0.1.3-1
- new package built with tito

* Sat Dec 31 2016 Ricardo Cordeiro <gryfrev8-redhat.com-rjmco@tux.com.pt> - 0.1.3-1
- Added python3 sub-package for the releases that have the necessary dependencies
- Updated URL and Source0 to HTTPS to follow guidelines

* Thu Mar 03 2016 Ricardo Cordeiro <gryfrev8-redhat.com-rjmco@tux.com.pt> - 0.1.3-1
- Initial package
